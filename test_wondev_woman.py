from unittest import main, TestCase
from wondev_woman import Woman, Direction, ImmutableBase, ImmutableBaseSlots


class PointSlots(ImmutableBaseSlots):
    __slots__ = 'x', 'y'

    def __init__(self, x: int, y: int):
        super().__init__(x, y)
        self.x = x
        self.y = y


class TestImmutableBaseSlots(TestCase):
    def test_immutability(self):
        a = PointSlots(1, 3)
        a.x = 0
        def x():
            a.prop2 = 0

        self.assertEqual(a.x, 0)
        self.assertRaises(AttributeError, x)
        self.assertRaises(AttributeError, lambda: a.prop2)

    def test_equality(self):
        a = PointSlots(1, 1)
        b = PointSlots(1, 2)
        c = PointSlots(1, 1)

        self.assertEqual(a, c)
        self.assertEqual(hash(a), hash(c))
        self.assertNotEqual(hash(b), hash(c))
        self.assertNotEqual(a, b)

    def test_repr(self):
        a = PointSlots(5, 8)
        #self.assertEquals('a', a.__slots__)
        self.assertEqual(repr(a), f'PointSlots(x=5, y=8)')


class TestImmutableBase(TestCase):
    class A(ImmutableBase):
        def __init__(self):
            super().__init__(prop=2)

    class Point(ImmutableBase):
        def __init__(self, x, y):
            super().__init__(x=x, y=y)

    def test_immutability(self):
        a = TestImmutableBase.A()
        a.prop = 0
        a.prop2 = 0

        self.assertEqual(a.prop, 2)
        self.assertRaises(AttributeError, lambda: a.prop2)

    def test_equality(self):
        a = TestImmutableBase.Point(1, 1)
        b = TestImmutableBase.Point(1, 2)
        c = TestImmutableBase.Point(1, 1)

        self.assertEqual(a, c)
        self.assertEqual(hash(a), hash(c))
        self.assertNotEqual(hash(b), hash(c))
        self.assertNotEqual(a, b)

    def test_repr(self):
        a = TestImmutableBase.Point(5, 8)

        self.assertEqual(repr(a), f'Point(x=5, y=8)')

    def test_woman(self):
        woman = Woman(0, 1, 6, 7)

        self.assertEqual(woman.index, 0)
        self.assertEqual(woman.team, 1)
        self.assertEqual(woman.x, 6)
        self.assertEqual(woman.y, 7)


class DirectionTests(TestCase):
    directions = (
        Direction.from_str('S'),
        Direction.from_str('SE'),
        Direction.from_str('W'),
    )

    def test_fmt(self):
        val = f'{Direction.from_str("S")}'

        self.assertEquals(val, 'S')

    def test_dir2str(self):
        s, se, w = map(str, DirectionTests.directions)
        self.assertEquals(s, 'S')
        self.assertEquals(se, 'SE')
        self.assertEquals(w, 'W')

    def test_str2dir(self):
        s, se, w = DirectionTests.directions
        self.assertEquals((s.dx, s.dy), (0, 1))
        self.assertEquals((se.dx, se.dy), (1, 1))
        self.assertEquals((w.dx, w.dy), (-1, 0))


if __name__ == '__main__':
    main()
