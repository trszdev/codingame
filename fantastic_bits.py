from sys import stderr
from typing import Iterable, List
from itertools import product
from math import acos


'''
Constraints
0 <= entityId <= 12
4 <= entities <= 13
0 < x < 16000
0 < y < 7500
-106 < vx < 106
-106 < vy < 106
0 <= state <= 1
0 <= thrust <= 150
0 <= power <= 500
'''


def read_ints() -> Iterable[int]:
    return map(int, input().split())


def debug(*values):
    print(values, file=stderr)


class Vector:
    def __init__(self, x: float, y: float):
        self.x = x
        self.y = y

    def dist(self) -> float:
        return abs(self.x) + abs(self.y)

    def mult(self, k: float) -> 'Vector':
        return Vector(self.x * k, self.y * k)

    def add(self, v: 'Vector') -> 'Vector':
        return Vector(self.x + v.x, self.y + v.y)

    def sub(self, v: 'Vector') -> 'Vector':
        return self.add(v.mult(-1))

    def norm_abs(self) -> 'Vector':
        d = self.dist() or 1e-10
        return self.mult(1 / d)

    def dist_abs_to(self, to: 'Vector') -> float:
        return self.sub(to).dist()

    def intvec(self) -> 'Vector':
        return Vector(int(self.x), int(self.y))

    def cross(self, v: 'Vector') -> float:
        return self.x * v.y - v.x * self.y

    def dot(self, v: 'Vector') -> float:
        return self.x * v.x + self.y * v.y

    def angle(self, v: 'Vector') -> float:
        return acos(self.dot(v) / (v.dist() * self.dist()))


class Command:
    @staticmethod
    def throw_to(v: Vector, force=500) -> str:
        v = v.intvec()
        return f'THROW {v.x} {v.y} {force}'

    @staticmethod
    def move_to(v: Vector, force=150) -> str:
        v = v.intvec()
        return f'MOVE {v.x} {v.y} {force}'

    @staticmethod
    def obliviate(id: int) -> str:
        """
            The wizard's team is invisible to the target bludger.
            Magic cost: 5
            Duration: 4
            Spell target: Bludgers
        """
        return f'OBLIVIATE {id}'

    @staticmethod
    def petrificus(id: int) -> str:
        """
            The target entity is immediately frozen to a standstill.
            Magic cost: 10
            Duration: 1
            Spell target: Bludgers, Snaffles, Opponent wizards
        """
        return f'PETRIFICUS {id}'

    @staticmethod
    def accio(id: int) -> str:
        """
            The target entity is pulled towards the wizard.
            Magic cost: 15
            Duration: 6
            Spell target: Bludgers, Snaffles
        """
        return f'ACCIO {id}'

    @staticmethod
    def flipendo(id: int) -> str:
        """
            The target entity is pushed away from the wizard.
            Magic cost: 20
            Duration: 3
            Spell target: Bludgers, Snaffles, Opponent wizards
        """
        return f'FLIPENDO {id}'


class Entity:
    def __init__(self, *args):
        # self.type: "WIZARD", "OPPONENT_WIZARD", "SNAFFLE", "BLUDGER"
        self.id, self.type, self.pos, self.vel, self.state = args

    @staticmethod
    def from_line(line: str) -> 'Entity':
        eid, etype, *other = line.split()
        x, y, vx, vy, state = map(int, other)
        return Entity(int(eid), etype, Vector(x, y), Vector(vx, vy), state)

    def __eq__(self, o: 'Entity') -> bool:
        if o is None:
            return False
        return self.id == o.id


def read_ents() -> List[Entity]:
    result = [Entity.from_line(input()) for _ in range(int(input()))]
    result.sort(key=lambda e: e.id)
    return result


MASS_WIZARD = 1
MASS_SNAFFLE = .5
MASS_BLUDGER = 8
BLUDGER_THRUST = 1000
FRICTION_WIZARD = .75
FRICTION_BLUDGER = .9
FRICTION_SNAFFLE = .75

is_orange = int(input())
goals = Vector(0, 3750), Vector(16000, 3750)
enemy_goal = goals[0 if is_orange else 1]
my_goal = goals[1 if is_orange else 0]
enemy_goals = [enemy_goal.add(Vector(0, y)) for y in range(0, 1300, 100)] + \
          [enemy_goal.sub(Vector(0, y)) for y in range(0, 1300, 100)]
my_goals = [my_goal.add(Vector(0, y)) for y in range(0, 1300, 100)] + \
          [my_goal.sub(Vector(0, y)) for y in range(0, 1300, 100)]


def sort_closest(wizard: Entity, balls: List[Entity]) -> List[Entity]:
    balls.sort(key=lambda b: b.pos.dist_abs_to(wizard.pos))
    return balls


def move_or_accio(wizard: Entity, ball: Entity):
    global my_magic
    enemy_is_near = any(x.pos.dist_abs_to(ball.pos) < 750 for x in opponents)
    ball_is_close = ball.pos.dist_abs_to(my_goal) < 2000
    req_magic = 15 if enemy_is_near or ball_is_close else 35
    if my_magic >= req_magic and wizard.pos.dist_abs_to(ball.pos) > 2000:
        my_magic -= 15
        print(Command.accio(ball.id))
    else:
        print(Command.move_to(ball.pos))


def ball_on_view(wizard: Entity) -> Entity:
    for ball in balls:
        nv = ball.pos.sub(wizard.pos).norm_abs()
        a, b = enemy_goal.add(Vector(0, 1400)), enemy_goal.sub(Vector(0, 1400))
        v1 = wizard.pos.sub(a)
        v2 = b.sub(a)
        v3 = Vector(-nv.y, nv.x)
        if abs(v2.dot(v3)) < 1e-5:
            continue
        t1 = v2.cross(v1) / v2.dot(v3)
        t2 = v1.dot(v3) / v2.dot(v3)
        ray_intersects = t1 >= 0 and 0 <= t2 <= 1
        if ray_intersects:
            for ent in opponents + bludgers:
                for pt in (nv.mult(x * 100).add(wizard.pos) for x in range(120)):
                    if ent.pos.dist_abs_to(pt) < 400:
                        break
            else:
                return ball


def attack_closest(wizard: Entity, aimed: Entity = None) -> Entity:
    global my_magic
    if wizard.state:
        bucket = min(enemy_goals, key=lambda b: b.dist_abs_to(wizard.pos))
        print(Command.throw_to(bucket))
    else:
        ball = ball_on_view(wizard)
        if ball and ball.pos.dist_abs_to(enemy_goal) > 1000 and ball.pos.dist_abs_to(wizard.pos) < 4000 and my_magic >= 20:
            my_magic -= 20
            print(Command.flipendo(ball.id))
        else:
            closest = sort_closest(wizard, balls)
            ball = [x for x in closest if x != aimed][0] if len(closest) > 1 else closest[0]
            move_or_accio(wizard, ball)
        return ball


def attack_defense(wizard: Entity, aimed: Entity = None) -> Entity:
    global my_magic
    if wizard.state:
        bucket = min(enemy_goals, key=lambda b: b.dist_abs_to(wizard.pos))
        print(Command.throw_to(bucket))
    else:
        ball = ball_on_view(wizard)
        if ball and ball.pos.dist_abs_to(enemy_goal) > 1000 and ball.pos.dist_abs_to(wizard.pos) < 4000 and my_magic >= 20:
            my_magic -= 20
            print(Command.flipendo(ball.id))
        else:
            balls.sort(key=lambda b: b.pos.dist_abs_to(my_goal))
            ball = [x for x in balls if x != aimed][0] if len(balls) > 1 else balls[0]
            move_or_accio(wizard, ball)
        return ball


def retarget_bludgers(wizard: Entity) -> bool:
    global my_magic
    targeted = [x for x in bludgers if x.state == wizard.id]
    if my_magic < 20:
        return False
    for a, b, c in product([wizard], opponents, targeted):
        if abs(a.pos.dist_abs_to(c.pos) - b.pos.dist_abs_to(c.pos)) <= 400:
            print(Command.obliviate(c.id))
            my_magic -= 5
            return True
    for bludger in targeted:
        if bludger.vel.dist() >= 300:
            print(Command.obliviate(bludger.id))
            my_magic -= 5
            return True
    return False


def freeze_balls(no_matter_waht_happens=False) -> bool:
    global my_magic
    for ball in balls:
        req_magic = 30 if ball.pos.dist_abs_to(my_goal) >= 1500 else 10
        if not no_matter_waht_happens and (ball.vel.dist() < 350 or my_magic < req_magic):
            continue
        for pt in (ball.vel.norm_abs().mult(x * 100).add(ball.pos) for x in range(100)):
            for goal in my_goals:
                if goal.dist_abs_to(pt) < 150:
                    print(Command.petrificus(ball.id))
                    my_magic -= 10
                    return True
    return False


def play(wizard: Entity, pal: Entity):
    if len(balls) == 1:
        ball, *_ = balls
        attacker = min(opponents, key=lambda o: o.pos.dist_abs_to(ball.pos))
        front = attacker.vel.norm_abs().mult(300).add(attacker.pos)
        attack_closest(wizard)
        if not freeze_balls():
            print(Command.move_to(front))
    else:
        p1 = attack_closest(wizard)
        if not freeze_balls():
            attack_defense(pal, p1)


while True:
    my_score, my_magic = read_ints()
    opponent_score, opponent_magic = read_ints()
    entities = read_ents()
    balls = [x for x in entities if x.type == 'SNAFFLE']
    wizards = [x for x in entities if x.type == 'WIZARD']
    bludgers = [x for x in entities if x.type == 'BLUDGER']
    opponents = [x for x in entities if x.type == 'OPPONENT_WIZARD']
    play(*wizards)
