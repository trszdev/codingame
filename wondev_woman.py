from collections import defaultdict
from sys import stderr
from time import time
from typing import Any, Tuple, Iterable, List, Optional, Set
from math import sqrt
from random import choice, randint


def debug(*args):
    print(*args, file=stderr)


class ImmutableBaseSlots:
    __slots__ = '___sealed', '___repr', '___hash'

    def __init__(self, *values):
        self.___sealed = tuple(values)
        self.___hash = hash(self.___sealed)
        desc = ', '.join([f'{k}={v}' for k, v in zip(self.__slots__, values)])
        self.___repr = f'{self.__class__.__name__}({desc})'

    def __repr__(self) -> str:
        return self.___repr

    def __eq__(self, other: 'ImmutableBaseSlots') -> bool:
        return other.___hash == self.___hash and other.___sealed == self.___sealed

    def __hash__(self) -> int:
        return self.___hash

    def __str__(self) -> str:
        return repr(self)


class ImmutableBase:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)
        self.__dict__['___frozen'] = tuple(kwargs.items())

    def __repr__(self) -> str:
        desc = ', '.join([f'{k}={v}' for k, v in self.__dict__.items() if k != '___frozen'])
        return f'{self.__class__.__name__}({desc})'

    def __eq__(self, other: 'ImmutableBase') -> bool:
        return self.__dict__['___frozen'] == other.__dict__['___frozen']

    def __hash__(self) -> int:
        return hash(self.__dict__['___frozen'])

    def __str__(self) -> str:
        return repr(self)

    def __setattr__(self, name: str, value: Any) -> None:
        pass


class Direction(ImmutableBaseSlots):
    __slots__ = 'dx', 'dy', 'val'

    def __init__(self, dx: int, dy: int, val: str):
        super().__init__(dx, dy, val)
        self.dx, self.dy, self.val = dx, dy, val

    def tuple(self):
        return self.dx, self.dy

    @staticmethod
    def from_str(s: str) -> 'Direction':
        ds = {'N': -1, 'S': 1, 'W': -1, 'E': 1}
        if len(s) == 1:
            d = ds[s]
            return Direction(0, d, s) if s in ['N', 'S'] else Direction(d, 0, s)
        else:
            return Direction(ds[s[1]], ds[s[0]], s)

    @staticmethod
    def from_tuple(dx: int, dy: int) -> 'Direction':
        val = {0: '', -1: 'N', 1: 'S'}[dy] + {0: '', 1: 'E', -1: 'W'}[dx]
        return Direction(dx, dy, val)

    def __str__(self) -> str:
        return self.val

    all = [(1, 1),
           (1, 0),
           (1, -1),
           (-1, 1),
           (-1, -1),
           (-1, 0),
           (0, -1),
           (0, 1)]  # type: Tuple[Direction, ...]


Direction.all = tuple(Direction.from_tuple(dx, dy) for dx, dy in Direction.all)


class Woman(ImmutableBaseSlots):
    __slots__ = 'index', 'team', 'x', 'y'

    def __init__(self, index: int, team: int, x: int, y: int):
        super().__init__(index, team, x, y)
        self.index = index
        self.team = team
        self.x, self.y = x, y

    def move(self, dx, dy) -> 'Woman':
        return Woman(self.index, self.team, self.x + dx, self.y + dy)


class Action(ImmutableBaseSlots):
    __slots__ = 't', 'index', 'd1', 'd2'

    def __init__(self, t: str, index: int, d1: Direction, d2: Direction):
        super().__init__(t, index, d1, d2)
        self.t = t
        self.index = index
        self.d1 = d1
        self.d2 = d2

    @staticmethod
    def read():
        return Action.parse(input())

    @staticmethod
    def parse(line):
        atype, index, *ds = line.split()
        return Action(atype[0], int(index), *map(Direction.from_str, ds))

    def __str__(self):
        return f'{"PUSH" if self.t == "P" else "MOVE"}&BUILD {self.index} {self.d1} {self.d2}'


class State(ImmutableBaseSlots):
    __slots__ = 'heightmap', 'women', 'scores'

    def __init__(self, heightmap: Tuple[Tuple[int, ...], ...], women: Tuple[Woman, ...], scores: Tuple[int, ...]):
        super().__init__(heightmap, women, scores)
        self.heightmap = heightmap
        self.women = women
        self.scores = scores

    @staticmethod
    def read(size: int, units_per_player: int, scores: List[int]) -> 'State':
        board = []
        for _ in range(size):
            board.append(tuple(int(x) if x != '.' else 100 for x in input()))
        women = []
        for i in range(units_per_player):
            x, y = map(int, input().split())
            women.append(Woman(i, 0, x, y))
        for i in range(units_per_player):
            x, y = map(int, input().split())
            if x == -1:
                x = -100
                y = -100
            women.append(Woman(i, 1, x, y))
        return State(tuple(board), tuple(women), tuple(scores))

    def height_at(self, x: int, y: int) -> int:
        if 0 <= y < len(self.heightmap) and 0 <= x < len(self.heightmap[0]):
            return self.heightmap[y][x]
        return 100

    def can_movebuild(self, windex: int, action: Action) -> bool:
        if not self.can_move(windex, action.d1):
            return False
        woman = self.women[windex]
        mx, my = action.d1.tuple()
        bx, by = action.d2.tuple()
        cx, cy = woman.x + mx + bx, woman.y + my + by
        new_height2 = self.height_at(cx, cy)
        built_on_unit = any((x.x, x.y) == (cx, cy) for x in self.women)
        return not built_on_unit and new_height2 < 4

    def movebuild(self, windex: int, action: Action) -> 'State':
        woman = self.women[windex]
        bx, by = action.d2.tuple()
        woman = woman.move(action.d1.dx, action.d1.dy)
        heightmap = list(self.heightmap)
        row = list(heightmap[woman.y + by])
        row[woman.x + bx] += 1
        heightmap[woman.y + by] = tuple(row)
        scores = list(self.scores)
        if self.height_at(woman.x, woman.y) == 3:
            scores[woman.team] += 1
        women = list(self.women)
        women[windex] = woman
        return State(tuple(heightmap), tuple(women), tuple(scores))

    def can_move(self, windex: int, dir: Direction) -> bool:
        woman = self.women[windex]
        cx, cy = woman.x + dir.dx, woman.y + dir.dy
        current_height = self.height_at(woman.x, woman.y)
        new_height = self.height_at(cx, cy)
        moved_on_unit = any((x.x, x.y) == (cx, cy) for x in self.women)
        return (not moved_on_unit and new_height < 4 and
                current_height >= new_height or new_height == current_height + 1)

    def can_pushbuild(self, windex: int, action: Action) -> bool:
        woman = self.women[windex]
        mx, my = action.d1.tuple()
        loc = woman.x + mx, woman.y + my
        pushed = [x for x in self.women if x.team != woman.team and (x.x, x.y) == loc]
        if not pushed:
            return False
        return self.can_move(pushed[0].index, action.d2)

    def pushbuild(self, windex: int, action: Action) -> 'State':
        woman = self.women[windex]
        mx, my = action.d1.tuple()
        bx, by = action.d2.tuple()
        loc = woman.x + mx, woman.y + my
        pushed, *_ = [(i, x) for i, x in enumerate(self.women)
                      if x.team != woman.team and (x.x, x.y) == loc]
        index, pushed = pushed
        women = list(self.women)
        women[index] = pushed.move(bx, by)

        heightmap = list(self.heightmap)
        row = list(heightmap[loc[1]])
        row[loc[0]] += 1
        heightmap[loc[1]] = tuple(row)

        return State(tuple(heightmap), tuple(women), self.scores)

    all_actions = None

    @staticmethod
    def get_all_actions():
        result = defaultdict(list)
        for d1 in Direction.all:
            for d2 in Direction.all:
                for i in range(2):
                    result[i].append(Action('M', i, d1, d2))
                    mx, my = d1.tuple()
                    bx, by = d2.tuple()
                    if (mx and my and mx != bx and my != by) or (mx and mx != bx) or (my and my != by):
                        continue
                    result[i].append(Action('P', i, d1, d2))
        return result

    def possible_moves_for_woman(self, windex: int) -> List[Action]:
        for action in State.all_actions[windex]:
            if action.t == 'M' and self.can_movebuild(windex, action):
                yield action
            elif self.can_pushbuild(windex, action):
                yield action

    def possible_moves_for_woman2(self, windex: int) -> int:
        result = 0
        woman = self.women[windex]
        for d1 in Direction.all:
            if self.can_move(windex, d1):
                mx, my = d1.tuple()
                for d2 in Direction.all:
                    bx, by = d2.tuple()
                    cx, cy = woman.x + mx + bx, woman.y + my + by
                    new_height2 = self.height_at(cx, cy)
                    built_on_unit = any((x.x, x.y) == (cx, cy) for x in self.women)
                    if not built_on_unit and new_height2 < 4:
                        result += 1
        for another_woman in self.women:
            if another_woman.team != woman.team:
                continue
            dx, dy = another_woman.x - woman.x, another_woman.y - woman.y
            if abs(dx) + abs(dy) <= 2:
                result += 1
        return result

    def possible_moves(self, team: int) -> Iterable[Action]:
        yield from self.possible_moves_for_woman(team * 2 + 1)
        yield from self.possible_moves_for_woman(team * 2)

    def apply_action(self, action: Action, team: int) -> Optional['State']:
        windex = (team * 2) + action.index
        if action.t == 'P':
            return self.pushbuild(windex, action)
        elif action.t == 'M':
            return self.movebuild(windex, action)

    def cost(self) -> float:
        cost = 0
        for i, woman in enumerate(self.women):
            mult = -1 if woman.team else 1
            #near_heights = [self.height_at(woman.x + dx, woman.y + dy) for dx, dy in
            #                map(Direction.tuple, Direction.all)]
            #possible_moves = len(list(self.possible_moves_for_woman(i)))
            current_height = self.height_at(woman.x, woman.y)
            #near_moves = [x for x in near_heights if x <= current_height]
            cost += mult * (.7 * self.possible_moves_for_woman2(i)  + current_height * 3.5)
        cost += 5 * self.scores[0]
        cost -= 5 * self.scores[1]
        return float(cost)

    def children(self, team: int) -> Iterable['State']:
        for action in self.possible_moves(team):
            yield self.apply_action(action, team)


def alphabeta(node: State, depth: int, alpha: int, beta: int, is_max: bool):
    if depth <= 0:
        return node.cost()
    v = 999999
    if is_max:
        v = -v
        children = list(node.children(0))
        if not children:
            return node.cost()
        for child in children:
            v = max(v, alphabeta(child, depth - 1, alpha, beta, False))
            alpha = max(alpha, v)
            if beta <= alpha:
                break
    else:
        children = list(node.children(1))
        if not children:
            return node.cost()
        for child in children:
            v = min(v, alphabeta(child, depth - 1, alpha, beta, True))
            beta = min(beta, v)
            if beta <= alpha:
                break
    return v


def alphabeta_choice(node: State, actions: List[Action], ctime: int) -> Action:
    v = -999999
    depth = int(3 * sqrt(len(actions)))
    alpha = -999999
    beta = 999999
    result = None
    for action, child in ((x, node.apply_action(x, 0)) for x in actions):
        ab = alphabeta(child, depth, alpha, beta, False)
        v = max(v, ab)
        if v > alpha:
            alpha = v
            result = action
        if time() - ctime > .0475:
            break
        if beta <= alpha:
            break
    return result


State.all_actions = State.get_all_actions()


class LoopFrame:
    def __init__(self):
        self.size = int(input())
        self.units_per_player = int(input())
        self.scores = [0, 0]
        self.prev_state = None
        self.state = None
        self.possible_positions = [set(), set()]
        self.prev_action = None

    def firstloop(self) -> None:
        self.state = State.read(self.size, self.units_per_player, self.scores)
        self.set_random_positions()
        legal_actions = int(input())
        legal_actions = [Action.read() for _ in range(legal_actions)]
        action = choice(legal_actions)
        self.scores = list(self.state.apply_action(action, 0).scores)
        debug([self.state.cost(), len(legal_actions), 0, 0])
        self.prev_action = action
        self.prev_state = self.state
        print(str(action))

    def loop(self) -> bool:
        self.update_scores()
        self.state = State.read(self.size, self.units_per_player, self.scores)
        self.clear_fog()

        ctime = time()
        legal_actions = int(input())
        if not legal_actions:
            return False
        legal_actions = [Action.read() for _ in range(legal_actions)]
        action = alphabeta_choice(self.state, legal_actions, ctime)
        self.scores = list(self.state.apply_action(action, 0).scores)

        debug([self.state.cost(), len(legal_actions), time() - ctime, self.scores[0] - self.scores[1]])
        print(str(action))
        self.prev_state = self.state
        self.prev_action = action
        return True

    def update_scores(self):
        for w in self.prev_state.women[2:]:
            if self.prev_state.height_at(w.x, w.y) == 3:
                self.scores[1] += 1

    def get_pvector(self) -> Tuple[int, int]:
        pwoman = self.prev_state.women[self.prev_action.index]
        px, py = pwoman.x, pwoman.y
        if self.prev_action.t == 'M':
            mx, my = self.prev_action.d1.tuple()
            bx, by = self.prev_action.d2.tuple()
            px += mx + bx
            py += my + by
        return px, py

    def set_random_positions(self) -> None:
        result = []
        women = list(self.state.women)
        for i in range(1000):
            x, y = randint(0, self.size - 1), randint(0, self.size - 1)
            if self.state.heightmap[y][x] > 3 or any((w.x, w.y) == (x, y) for w in women if w.x >= 0):
                continue
            result.append((x, y))
        if len(result) != 2:
            return
        a, b = women[2], women[3]
        if a.x < 0:
            women[2] = Woman(a.index, a.team, *result[0])
        if b.x < 0:
            women[3] = Woman(b.index, b.team, *result[1])
        self.state = State(self.state.heightmap, tuple(women), self.state.scores)

    def get_possible_positions(self) -> List[Tuple[int, int]]:
        pvector = self.get_pvector()
        result = []
        for y, prev_row in enumerate(self.prev_state.heightmap):
            for x, prev_cell in enumerate(prev_row):
                cell = self.state.heightmap[y][x]
                if cell != prev_cell and (x, y) != pvector:
                    for dir in Direction.all:
                        pos = dir.dx + x, dir.dy + y
                        if self.state.heightmap[y][x] > 3 or \
                                any((w.x, w.y) == pos for w in self.state.women if w.x >= 0):
                            continue
                        result.append(pos)
                    return result
        return []

    def clear_fog(self) -> None:
        pos = self.get_possible_positions()
        if not pos:
            return
        px, py = choice(pos)
        women = list(self.state.women)
        a, b = self.prev_state.women[2:]
        c, d = self.state.women[2:]
        ax, ay = a.x - px, a.y - py
        bx, by = b.x - px, b.y - py
        if (abs(ax) + abs(ay)) > (abs(bx) + abs(by)):
            if d.x < 0:
                women[3] = Woman(b.index, b.team, px, py)
                women[2] = a if c.x < 0 else c
            elif c.x < 0:
                women[2] = Woman(a.index, a.team, px, py)
                women[3] = b if d.x < 0 else d
        else:
            if c.x < 0:
                women[2] = Woman(a.index, a.team, px, py)
                women[3] = b if d.x < 0 else d
            elif d.x < 0:
                women[3] = Woman(b.index, b.team, px, py)
                women[2] = a if c.x < 0 else c
        self.state = State(self.state.heightmap, tuple(women), self.state.scores)


def main():
    frame = LoopFrame()
    frame.firstloop()
    while frame.loop():
        pass


if __name__ == '__main__':
    main()
