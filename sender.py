from requests import Session
from sys import argv
from typing import Dict, List, Optional
from wondev_woman import ImmutableBase
import matplotlib.pyplot as plt


FANTASTIC_BITS_ID = '1027257892aca02a4543245b24f2cf6480504d88'
WONDEVWOMAN_ID = '1032562958449491d3327e89516c815f5eefa987'


class Frame(ImmutableBase):
    def __init__(self, agent_id: int, game_information: str, is_keyframe: bool,
                 stdout: Optional[str], stderr: Optional[str]):
        super().__init__(agent_id=agent_id, game_information=game_information,
                         is_keyframe=is_keyframe, stdout=stdout, stderr=stderr)
        self.agent_id = agent_id
        self.game_information = game_information
        self.is_keyframe = is_keyframe
        self.stdout = stdout
        self.stderr = stderr


class GameResult(ImmutableBase):
    def __init__(self, scores: List[int], frames: List[Frame], game_id: int):
        super().__init__(scores=scores, frames=frames, game_id=game_id)
        self.scores = scores
        self.frames = frames
        self.game_id = game_id


def login(s: Session, login: str, password: str) -> Dict:
    url = 'https://www.codingame.com/services/CodingamerRemoteService/loginSiteV2'
    return s.post(url, json=[login, password, True]).json()


def play(s: Session, game_id: str, code: str, game_options: Optional[str] = None) -> Dict:
    url = 'https://www.codingame.com/services/TestSessionRemoteService/play'
    payload = [game_id, {
        'code': code,
        'multi': {'agentsIds': [-1, -2], 'gameOptions': game_options},
        'programmingLanguageId': 'Python3'
    }]
    return s.post(url, json=payload).json()


def read_code(filename: str) -> str:
    with open(filename) as file:
        return file.read()


def parse_results(result: Dict) -> Optional[GameResult]:
    if 'success' not in result:
        return
    result = result['success']
    frames = []
    for frame in result['frames']:
        frames.append(Frame(frame['agentId'], frame['gameInformation'], frame['keyframe'],
                            frame.get('stdout', ''), frame.get('stderr', '')))
    return GameResult([*map(int, result['scores'])], frames, result['gameId'])


def main_wv(*args):
    _, games_to_play, username, password = args
    games_to_play = int(games_to_play)
    s = Session()
    print('Starting')
    if 'error' in login(s, username, password):
        raise PermissionError('Login failed')
    print('Logged in')
    code = read_code('wondev_woman.py')
    game_results = []
    for x in range(1, games_to_play+1):
        print(f'Playing {x}th game')
        game_result = play(s, WONDEVWOMAN_ID, code)
        parsed = parse_results(game_result)
        if parsed:
            game_results.append(parsed)
        else:
            print('FAIL', game_result)

    frames = [len(x.frames) * (1 if x.scores[0] > x.scores[1] else -1) for x in game_results]
    scores = [x.scores[0] - x.scores[1] for x in game_results]
    stats = [[eval(y.stderr) for y in x.frames if y.agent_id == 0 and y.stderr] for x in game_results]
    cost = [[y[0] for y in x] for x in stats if x]
    actions = [[y[1] for y in x] for x in stats if x]
    time = [[y[2] * 1000 for y in x] for x in stats if x]
    delta_score = [[y[3] for y in x] for x in stats if x]

    plt.subplot(2, 3, 1)
    plt.axhline(0, color='black', lw=2)
    plt.plot(frames, 'o-', label='Frames')
    plt.legend()

    plt.subplot(2, 3, 2)
    plt.axhline(0, color='black', lw=2)
    plt.plot(scores, 'x-', label='DeltaScore')
    plt.legend()

    for i, name, metric, baseline in [
        (3, 'cost', cost, 0),
        (4, 'actions', actions, None),
        (5, 'time (ms)', time, 50),
        (6, 'delta score', delta_score, 0)
    ]:
        plt.subplot(2, 3, i)
        plt.plot([sum(y for y in x) / len(x) for x in metric], 'r-', label=f'Avg.{name}')
        plt.plot([min(x) for x in metric], 'g-', label=f'Min.{name}')
        plt.plot([max(x) for x in metric], 'b-' if i != 5 else 'bx-', label=f'Max.{name}')
        if baseline is not None:
            plt.axhline(baseline, color='black', lw=2)
        plt.legend()

    plt.show()


if __name__ == '__main__':
    main_wv(*argv)

